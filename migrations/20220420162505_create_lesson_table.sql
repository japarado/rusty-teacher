-- Add migration script here
CREATE TABLE lesson
(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(255) NOT NULL,
    description TEXT
)
