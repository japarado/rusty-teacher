use std::env;
use std::fs::File;
use std::io::{self, BufReader};

use actix_cors::Cors;
use actix_identity::{CookieIdentityPolicy, IdentityService};
use actix_web::middleware::NormalizePath;
use actix_web::web::Data;
use actix_web::{get, web, App, HttpResponse, HttpServer};
use dotenv::dotenv;
use rustls::ServerConfig;
use rustls::{Certificate, PrivateKey};
use rustls_pemfile::{certs, pkcs8_private_keys};
use sqlx::postgres::PgPoolOptions;
use sqlx::{Pool, Postgres};

use auth::auth_routes::config as auth_routes;
use lessons::lesson_routes::config as lessons_routes;

mod auth;
mod errors;
mod lessons;
mod repositories;

pub async fn start_server() -> io::Result<()>
{
    dotenv().ok();

    let config = load_rustls_config();

    let app_data = prepare_app_data().await;

    let server = HttpServer::new(move || {
        let cors = Cors::default()
            .allow_any_origin()
            .allow_any_method()
            .allow_any_header()
            .supports_credentials()
            .max_age(3600);

        let policy = CookieIdentityPolicy::new(&[0; 32])
            .name("auth-cookie")
            .http_only(!is_using_tls()) // false for https
            .secure(is_using_tls()); // true for https

        App::new()
            .app_data(app_data.clone())
            .wrap(cors)
            .wrap(NormalizePath::default())
            .wrap(IdentityService::new(policy))
            .wrap(NormalizePath::default())
            .service(
                web::scope("/api")
                    .configure(lessons_routes)
                    .configure(auth_routes),
            )
            .service(ping)
    });
    if is_using_tls()
    {
        println!("Using TLS");
        server.bind_rustls("127.0.0.1:8000", config)?.run().await
    }
    else
    {
        println!("No TLS");
        server.bind(("127.0.0.1", 8000))?.run().await
    }
}

#[get("ping")]
async fn ping() -> HttpResponse
{
    let message = "PONG";
    println!("{}", message);
    HttpResponse::Ok().json(message)
}

fn is_using_tls() -> bool
{
    env::var("USE_TLS").unwrap_or_default() == "true"
}

fn load_rustls_config() -> rustls::ServerConfig
{
    let config = ServerConfig::builder()
        .with_safe_defaults()
        .with_no_client_auth();

    let cert_file = &mut BufReader::new(File::open("./cert.pem").unwrap());
    let key_file = &mut BufReader::new(File::open("./key.pem").unwrap());

    let cert_chain = certs(cert_file)
        .unwrap()
        .into_iter()
        .map(Certificate)
        .collect();
    let mut keys: Vec<PrivateKey> = pkcs8_private_keys(key_file)
        .unwrap()
        .into_iter()
        .map(PrivateKey)
        .collect();

    if keys.is_empty()
    {
        eprintln!("Could not locate PKCS 8 private keys.");
        std::process::exit(1)
    }

    config.with_single_cert(cert_chain, keys.remove(0)).unwrap()
}

async fn prepare_app_data() -> Data<AppData>
{
    let auth_config = AuthConfig {
        salt: env::var("PASSWORD_SALT").unwrap(),
    };

    let db_connection_pool = PgPoolOptions::new()
        .max_connections(5)
        .connect("postgres://pam:pam@localhost/rusty-teacher")
        .await
        .unwrap();

    Data::new(AppData {
        auth_config,
        db_connection_pool,
    })
}

// TODO: Remove from AppData and turn into an env var instead
pub struct AppData
{
    pub auth_config: AuthConfig,
    pub db_connection_pool: Pool<Postgres>,
}

pub struct AuthConfig
{
    pub salt: String,
}
