use derive_more::{Display, Error};

/// Meant to be seen by the API consumers
#[derive(Debug, Display, Error)]
#[display(fmt = "my error: {}", name)]
pub struct UserError
{
    pub name: &'static str,
}

/// Meant to be seen internally e.g. logs
struct _InternalError {}

struct _AppError
{
    name: &'static str,
    code: &'static str,
    message: &'static str,
}
