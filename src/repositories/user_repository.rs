use crate::auth::auth_models::request::SignupRequest;
use crate::{Pool, Postgres};
use argon2::Config;
use serde::Serialize;
use time::PrimitiveDateTime;

#[derive(Debug, Serialize)]
pub struct UserRow
{
    pub id: i32,
    pub email: String,
    pub password: String,
    pub created_at: Option<PrimitiveDateTime>,
}

pub async fn store_user(
    conn: &Pool<Postgres>,
    user_details: SignupRequest,
) -> Result<UserRow, sqlx::Error>
{
    sqlx::query_as!(
        UserRow,
        r#"INSERT INTO app_user (email, password) VALUES ($1, $2) RETURNING *"#,
        user_details.email,
        hash_password(&user_details.password)
    )
    .fetch_one(conn)
    .await
}

fn hash_password(password: &str) -> String
{
    let salt = b"randomsalt";
    let config = Config::default();
    let hash = argon2::hash_encoded(password.as_ref(), salt, &config).unwrap();
    hash
}
