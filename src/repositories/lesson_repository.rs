use serde::Serialize;
use sqlx::{Pool, Postgres};

use crate::lessons::lesson_models::request::CreateLessonRequest;

#[derive(Debug, Serialize)]
pub struct LessonRow
{
    pub id: i32,
    pub name: String,
    pub description: Option<String>,
}

pub async fn index(conn: &Pool<Postgres>) -> Result<Vec<LessonRow>, sqlx::Error>
{
    sqlx::query_as!(LessonRow, "SELECT * FROM lesson")
        .fetch_all(conn)
        .await
}

pub async fn get(conn: &Pool<Postgres>, id: i32) -> Result<LessonRow, sqlx::Error>
{
    sqlx::query_as!(LessonRow, r#"SELECT * FROM lesson WHERE id = $1"#, id)
        .fetch_one(conn)
        .await
}

pub async fn store(
    conn: &Pool<Postgres>,
    lesson: CreateLessonRequest,
) -> Result<LessonRow, sqlx::Error>
{
    sqlx::query!(
        "INSERT INTO lesson (name, description) VALUES ($1, $2) RETURNING id, name, description",
        lesson.name,
        lesson.description
    )
    .map(|lesson| LessonRow {
        id: lesson.id,
        name: lesson.name,
        description: lesson.description,
    })
    .fetch_one(conn)
    .await
}
