use rusty_teacher::start_server;
use std::io;

#[actix_web::main]
async fn main() -> io::Result<()>
{
    start_server().await
}
