use actix_web::web;

use crate::auth::auth_handlers;

pub fn config(cfg: &mut web::ServiceConfig)
{
    cfg.service(
        web::scope("")
            .service(auth_handlers::login)
            .service(auth_handlers::logout)
            .service(auth_handlers::signup)
            .service(auth_handlers::me),
    );
}
