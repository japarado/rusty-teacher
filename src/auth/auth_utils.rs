use actix_identity::Identity;

use crate::auth::auth_models::request::LoginUserRequest;

pub fn get_auth_user(id: &Identity) -> Option<LoginUserRequest>
{
    id.identity().map(|identity| serde_json::from_str::<LoginUserRequest>(&identity).unwrap())
}
