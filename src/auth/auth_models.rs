pub mod request
{
    use actix_web::web;
    use actix_web::web::Json;
    use serde::{Deserialize, Serialize};

    #[derive(Deserialize)]
    pub struct SignupRequest
    {
        pub email: String,
        pub password: String,
        pub password_confirm: String,
    }

    impl SignupRequest
    {
        pub fn is_pass_confirm_correct(&self) -> bool
        {
            self.password == self.password_confirm
        }
    }

    impl From<web::Json<SignupRequest>> for SignupRequest
    {
        fn from(request: Json<SignupRequest>) -> Self
        {
            Self {
                email: request.email.clone(),
                password: request.password.clone(),
                password_confirm: request.password.clone(),
            }
        }
    }

    #[derive(Debug, Serialize, Deserialize)]
    pub struct LoginUserRequest
    {
        pub username: String,
        pub password: String,
    }
}

pub mod response
{
    use serde::Serialize;

    #[derive(Serialize)]
    pub struct AuthUserResponse
    {
        pub name: String,
    }

    #[derive(Serialize)]
    pub struct SignupResponse
    {
        pub id: i32,
        pub email: String,
        pub created_at: String,
    }
}
