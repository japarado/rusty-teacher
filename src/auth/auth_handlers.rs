use std::borrow::Borrow;

use actix_identity::Identity;
use actix_web::web::Json;
use actix_web::{delete, get, post, web, HttpResponse, Responder};

use crate::auth::auth_models::request::{LoginUserRequest, SignupRequest};
use crate::auth::auth_models::response::AuthUserResponse;
use crate::repositories::user_repository;
use crate::AppData;

#[post("/signup")]
pub async fn signup(payload: Json<SignupRequest>, data: web::Data<AppData>) -> impl Responder
{
    if payload.is_pass_confirm_correct()
    {
        let conn = data.db_connection_pool.borrow();

        let query_result = user_repository::store_user(conn, payload.into())
            .await
            .unwrap();

        println!("{:#?}", query_result);

        HttpResponse::Ok().json("Ok")
    }
    else
    {
        HttpResponse::BadRequest().json("Passwords do not match")
    }
}

#[post("/login")]
pub async fn login(id: Identity, payload: Json<LoginUserRequest>) -> HttpResponse
{
    let auth_user = payload.into_inner();
    let user_as_string = serde_json::to_string(&auth_user).unwrap();
    id.remember(user_as_string);
    HttpResponse::Ok().json(auth_user)
}

#[delete("/logout")]
pub async fn logout(id: Identity) -> HttpResponse
{
    id.forget();
    HttpResponse::Ok().finish()
}

#[get("/me")]
pub async fn me(id: Identity) -> HttpResponse
{
    HttpResponse::Ok().json(AuthUserResponse {
        name: "Just another user".to_string(),
    });

    if let Some(i) = id.identity()
    {
        let user_data = serde_json::from_str::<LoginUserRequest>(&i).unwrap();
        println!("{:?}", user_data);
        HttpResponse::Ok().json(user_data)
    }
    else
    {
        HttpResponse::Forbidden().json("You are not logged in")
    }
}
