pub mod response
{
    use crate::repositories::lesson_repository::LessonRow;

    pub type _GetLessonResponse = LessonRow;

    pub type _CreateLessonResponse = LessonRow;
}

pub mod request
{
    use serde::Deserialize;

    #[derive(Deserialize)]
    pub struct CreateLessonRequest
    {
        pub name: String,
        pub description: Option<String>,
    }
}
