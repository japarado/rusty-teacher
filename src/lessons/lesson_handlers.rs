use std::borrow::Borrow;

use actix_web::{get, post, web, HttpRequest, HttpResponse, Responder};

use crate::lessons::lesson_models::request::CreateLessonRequest;
use crate::repositories::lesson_repository;
use crate::AppData;

#[get("")]
pub async fn index(_req: HttpRequest, data: web::Data<AppData>) -> impl Responder
{
    let conn = data.db_connection_pool.borrow();
    let lessons = lesson_repository::index(conn).await.unwrap();
    HttpResponse::Ok().json(lessons)
}

#[get("/{id}")]
pub async fn get(path: web::Path<i32>, data: web::Data<AppData>) -> impl Responder
{
    let conn = data.db_connection_pool.borrow();
    let lesson = lesson_repository::get(conn, path.into_inner())
        .await
        .unwrap();
    HttpResponse::Ok().json(lesson)
}

#[post("")]
pub async fn store(
    payload: web::Json<CreateLessonRequest>,
    data: web::Data<AppData>,
) -> impl Responder
{
    let conn = data.db_connection_pool.borrow();
    let lesson = lesson_repository::store(conn, payload.into_inner())
        .await
        .unwrap();
    HttpResponse::Ok().json(lesson)
}
