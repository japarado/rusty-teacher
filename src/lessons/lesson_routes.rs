use actix_web::web;

use crate::lessons::lesson_handlers;

pub fn config(cfg: &mut web::ServiceConfig)
{
    cfg.service(
        web::scope("lessons")
            .service(lesson_handlers::get)
            .service(lesson_handlers::index)
            .service(lesson_handlers::store),
    );
}
